- url: mission-mobility
  title: "Mission Mobility: A DevSecOps Discussion"
  date: March 26, 2019 at 12pm ET / 5pm UTC
  form: 1592
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/webcast/public-sector.svg
  description: "Join Bill Nystrom, CTO of Air Combat Command Directorate of Communications, Harold Smith III, Co-Founder and CEO of Monkton, Pradeeb Chhabra, Director, Security Engineering at Capital One, and John Jeremiah, Product Marketing Leader at GitLab in a panelist discussion around improving your speed to market in a highly regulated environment."
  content: |

    Building and delivering secure mobile apps in a regulated environment is not easy. Policies, regulations, and laws combined make quick delivery harder - if not impossible for most organizations.
    
    Interested in learning how to improve your speed to market while still ensuring your apps are secure and compliant?
    
    Join Bill Nystrom, CTO of Air Combat Command Directorate of Communications, Harold Smith III, Co-Founder and CEO of Monkton, Pradeeb Chhabra, Director, Security Engineering at Capital One, and John Jeremiah, Product Marketing Leader at GitLab for an interactive discussion.

    ### During the discussion, we will cover:
    * User focus
    * Considerations in the DevSecOps life cycle
    * Benefits of automation in ensuring continuous delivery of secure and compliant apps

  speakers:
    - name: "Bill Nystrom"
      title: "CTO,  Air Combat Command Directorate of Communications"
      about: "Mr. William T. Nystrom is the Chief Technology Officer assigned to the Head Quarters Air Combat Command Communications Directorate located at Langley Air Force Base, Virginia. In this capacity, Mr. Nystrom supports numerous Air Force command and control, computer, communications, and cyberspace operations activities in direct support of Combat Air Force (CAF) requirements. He is considered the MAJCOM IT subject matter expert, lead integrator, and technical champion ensuring IT efforts are prioritized, resourced, integrated, and synchronized to advance current and future Air Force operations."
      image:  /images/webcast/bill-nystrom.png
    - name: "Harold Smith III"
      title: "Co-Founder & CEO, Monkton"
      about: "Harold Smith III is the CEO and co-founder of Monkton, Inc. Monkton's focus is helping enterprises build and deliver secure mobile software in a repeatable fashion by leveraging Monkton Rebar. Harold's day to day includes architecture, design, and helping customers deliver mission solutions. Previously, Harold started RxmindMe, which was acquired by Walgreens in 2011. In 2012, Fixmo, an NSA CRADA company, acquired Harold's second mobile security startup. Harold spent several years working directly with the U.S. Government on various projects at the USMC and Department of State, offering a unique view into the issues facing our government, our military, our warfighters, and our citizens."
      image: /images/webcast/harold-smith.jpg
    - name: "Pradeep Chhabra - CISSP, CISM, CISA, CRISC"
      title: "Director, Security Engineering, Capital One"
      about: "Pradeep Chhabra has 22 years of IT experience including 12 years in Information Security. Currently, Pradeep works as a Director of Security engineering in Capital One where he is responsible for automating security controls in mobile and web CI/CD. Pradeep is “shifting security left” by building security controls earlier into design, coding and the automated test cycles. Pradeep firmly believes that security must fit into the way that engineers think and work - more iterative and incremental, automated in ways that are efficient, repeatable, and easy to use. Before joining Capital One, Pradeep used to work in JPMorgan Chase where he served as Head of Application Security for Chase Credit Cards and played a key role in implementing Thread Modeling and Red team programs. Pradeep currently holds CISSP, CISA, CISM and CRISC certifications."
      image:  /images/webcast/pradeep-chhabra.jpg
    - name: "John Jeremiah"
      title: "Enterprise DevOps evangelist and product marketing leader, GitLab"
      about: "John is an enterprise DevOps evangelist and product marketing leader at GitLab with over 20 years of IT leadership and software development experience. John helps to guide customers to adapt and thrive in a world of rapid change and digital disruption. He has held a variety of leadership roles with the U.S. Navy, IT consulting and Fortune 500 IT organizations and his experience spans from application developer, project and program manager, and IT Director where he has led the adoption of an Agile and CMMI Maturity Level 3 process framework. He has spoken at numerous conferences and events ranging from Interop to TedX. After receiving a bachelor’s degree from Oregon State University, Jeremiah earned his MS in Information Technology from The George Washington University School of Business and served nine years as a Naval Officer with the United States Navy."
      image: /images/team/johnjeremiah-crop.jpg

- url: cloud-native-transformation
  title: "A Cloud Native Transformation"
  subtitle: "Learn how Ask Media Group modernized their architecture and development with microservices, containers, and Kubernetes. "
  date: March 6,  10am PT / 6pm UTC
  form: 1592
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/icons/just-commit-flying-boxes.svg
  description: "Over the past few years, Ask Media Group has made the shift from on-prem data centers to the AWS cloud. In doing so, they’ve rearchitected numerous legacy systems from monoliths to microservices. Hear from Chenglim Ear, Principal Software Engineer, Ask Media Group about how they accomplished this and what lessons they can share with you."
  content: |

    Over the past few years, Ask Media Group has made the shift from on-prem data centers to the AWS cloud. In doing so, they’ve rearchitected numerous legacy systems from monoliths to microservices. An underlying architecture based on containers, leveraging Gitlab Runners, Kubernetes, Docker, Linkerd and AWS was key to this migration. Beyond the architectural changes, the teams at Ask also changed how they worked, and leveraged GitLab as the primary mechanism to configure, build, test, deploy, and manage their microservices. GitLab essentially became the front-end for this new infrastructure.
    
    During this discussion, hear from Chenglim Ear, Principal Software Engineer, Ask Media Group about how they accomplished this and what lessons they can share with you.

    ### What we will cover:
    * Learn which organizational, cultural, and technical barriers block modernization and how Ask Media Group overcame them
    * Understand the benefits and business outcomes that result from a monolith to microservices journey
    * Get a technical deep dive on using GitLab CI/CD, Kubernetes, and AWS to power a cloud native architecture

  speakers:
    - name: "Chenglim Ear"
      title: "Principal Software Engineer, AskMedia Group"
      about: "Chenglim is a developer from the early days of cloud computing when it was first used when mobile applications and phones first gained access to the internet.  At Ask Media Group, Chenglim leads the development of microservices in the cloud, utilizing modern tools and platforms such as GitLab, Kubernetes, Docker and AWS.  He is particularly interested in the idea of building systems using building blocks.  When he’s not programming, you’ll find Chenglim running, swimming, biking and sharing a green drink with his family."
      image:  /images/webcast/cheng-lim.png
    - name: "Trevor Hansen"
      title: "Partner Solution Architect, Amazon Web Services"
      about: "Trevor Hansen is a Startup Partner Solutions Architect at Amazon Web Services (AWS). He has worked in a wide range of technical roles which includes driving the pace of innovation with startups, through to helping enterprises with their digital transformation and cloud journey. His startup work has got him working closely with some of the world’s most recognized technology startups, accelerators, incubators and venture capitalist firms. Now he is primarily focused on being the technical point of contact for VC Backed Startups and working closely with the Startup Business Development teams to help partners architect cloud-based solutions, drive technical enablement and integrations, digital transformation, got-to-market strategies and global expansion."
      image: /images/webcast/trevor-hansen.jpg
    - name: "William Chia"
      title: "Sr. Product Marketing Manager, GitLab"
      about: "William is a developer and technical marketer who’s been crafting both code and copy since the late 90’s. At GitLab, William leads product marketing for all of GitLab's ops functionality including CI/CD, Kubernetes, and GitLab Serverless. Prior to joining GitLab, William worked in product marketing at Twilio and Digium. Outside of work, you can find William cooking, skateboarding, and playing video games with his wife and three children."
      image: /images/team/william-chia-crop.jpg

- url: weekly-demo
  title: "Join us for GitLab demo and see how you can accelerate your DevOps lifecycle"
  subtitle: "See how GitLab works during a live Q&A"
  date: Wednesdays,  11am PT / 7pm UTC
  form: 1763
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/demo/demo.svg
  description: "This 30-minute session includes a demonstration of how a change moves through the entire DevOps lifecycle using GitLab from Planning to Monitoring, along with a live Q&A!"
  content: |

    Curious to see how GitLab works? Join us for a 30-minute session where a product marketing manager will walkthrough a change in GitLab during the entire DevOps lifecycle using GitLab from Planning to Monitoring, along with a live Q&A!

    GitLab is the only single application for the entire DevOps lifecycle. Only GitLab provides everything you need to Manage, Plan, Create, Verify, Package, Release, Configure, Monitor, and Secure your applications. This makes the software lifecycle 200% faster, radically improving the speed of business.

    ### In the session, we'll:
    * Show GitLab’s built-in issue tracking, issue board, version control, code review, Auto DevOps, CI/CD, SAST/DAST and more!
    * Host a live Q&A session where you can ask your questions and get answers from a GitLab product expert during the demo.

- url: whats-next-for-gitlab
  title: "Livestream: What’s next for GitLab?"
  youtube_url: https://www.youtube.com/embed/ZgFqyXCsqPY
  subtitle: Join us for a special <a href="https://twitter.com/search?q=%23gitlablive" target="_blank">#GitLabLive</a> event
  date: September 20, 1pm ET / 5pm UTC
  form: 1592
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/webcast/gitlab-live.svg
  description: "Two years ago we held our very first #GitLabLive event where we unveiled our master plan to build a complete toolset for developers, from idea to production. Since then, we’ve not only delivered on that vision, but expanded our product to include the entire DevOps lifecycle. We’ve seamlessly integrated each step, so dev and ops teams can plan, create, verify, package, release, configure, monitor, and secure software within a single application."
  content: |

    Two years ago we held our very first #GitLabLive event where we unveiled our master plan to build a complete toolset for developers, from idea to production. Since then, we’ve not only delivered on that vision, but expanded our product to include the entire DevOps lifecycle. We’ve seamlessly integrated each step, so dev and ops teams can plan, create, verify, package, release, configure, monitor, and secure software within a single application.

    But we’re not stopping there.

    Our mission is to make it easy for everyone to contribute. That’s why we want to include security, designers, project managers, and executives as first class citizens in our product. With all stakeholders working from the same interface, sharing the same data-store, teams can work concurrently, eliminating the need for handoffs, and unleashing the power of better collaboration to drive results faster.

    Join us for a special live event to find out what’s next for GitLab and how we’re going to get there. Plus, a few surprises that we can’t wait to share!

    ### What to expect:
    * Welcome
    * Company update
    * Scaling a remote culture
    * Special guests
    * Product update & vision
    * Q&A

  speakers:
    - name: "Sid Sijbrandij"
      title: "CEO, GitLab"
      about: "Sid saw the first ruby code in 2007 and loved it so much he learned to program and made this his profession. Before that he was engaged in various businesses, from selling programmable infrared receivers to starting a recreational submarine company. He is always looking to publicly document an answer or to make a process more efficient. He was never good at sports but tries to make it up in quantity by hiking, sailing, skiing, snowboarding, speed skating, dancing Zouk, squashing, and running. He loves participating in conversations on Hacker News, and reading Marc Andreessen's tweets (now that Marc stopped this greatly increased his productivity)."
      image: /images/team/picture_sytse-crop.jpg
    - name: "Barbie Brewer"
      title: "Chief Culture Officer, GitLab"
      about: "Before joining GitLab in September 2017, Barbie worked as a consultant for tech companies internationally, after spending 6 years as Netflix's VP of Talent. Barbie loves helping companies build amazing cultures and workplaces that are 100% aligned to the success of the business and is excited to give her full attention to GitLab. Barbie is also an avid reader who also enjoys SCUBA, Golf, and cooking. She spends most of her weekends watching her kids play sports or heading to the beach, as she has recently taken up surfing. Barbie is very excited about the products that GitLab offers and the unique culture and workforce. Having lived in three countries and traveled the planet in previous roles, the idea of a remote workforce is thrilling to her. Barbie believes that if GitLab can continue to build an amazing product with a remote workforce, the learnings can be transferred to others. Successful remote workforces strengthen individuals, families, and communities. Barbie also hopes that a remote workforce will expand the opportunity to have a diverse set of candidates and employees."
      image: /images/team/barbie-crop.jpg
    - name: "Mark Pundsack"
      title: "Head of Product, GitLab"
      about: "Originally a computer engineer doing speech recognition research, he moved to product management about 20 years ago. Over the past six years, Mark has developed a passion for developer experience. Before GitLab, Mark was at CircleCI as VP of Product, and before that, at Heroku focusing on continuous delivery and the Heroku-GitHub integration. Outside of work he builds Star Wars Lego with his 6-year-old son and dresses up his 3-year-old daughter as a princess. A recent transplant to Chicagoland from California, you'll often see him bundled up in a winter coat for no reason at all."
      image: /images/team/mpundsack-crop.jpg
    - name: "William Chia"
      title: "Manager, Product Marketing, Ops"
      about: "William is a technical marketer who’s been crafting both code and copy since the late 90’s. Prior to joining GitLab, William worked in product marketing at Twilio and Digium. Outside of work, you can find William cooking, skateboarding, and playing video games with his wife and three children."
      image: /images/team/william-chia-crop.jpg

- url: keys-to-devops-success
  youtube_url: https://www.youtube.com/embed/dbkj0qXQ22A
  title: "Keys to DevOps Success"
  subtitle: "Findings from The 2018 Accelerate State of DevOps Report"
  date: October 11th, 9am PDT / 4pm UTC
  form: 1592
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/webcast/devops-loop.svg
  description: "Accelerating and streamlining software delivery is a common challenge faced by almost every IT leader. We have partnered with Gene Kim and the DevOps Research and Assessment team to create this year’s version of 'The Accelerate State of DevOps Report'.  This report is the result of five years of research--with over 30,000 data points--to understand high performance in the context of software development, and the factors that predict it."
  content: |

    Accelerating and streamlining software delivery is a common challenge faced by almost every IT leader. We have partnered with Gene Kim and the DevOps Research and Assessment team to create this year’s version of 'The Accelerate State of DevOps Report'.  This report is the result of five years of research--with over 30,000 data points--to understand high performance in the context of software development, and the factors that predict it.

    Join us as we discuss the latest key findings in the 2018 report and share observations from the GitLab team, where we are practicing DevOps and helping our customers accelerate their software delivery.

    ### What we cover:
    * Software delivery performance unlocks several competitive advantages including increased profitability, productivity, market share, customer service, and ability to achieve organization or mission goals.
    * Not just using cloud infrastructure, but implementing cloud as it’s intended to be used, contributes to high performance.
    * Outsourcing may save money in the short term, but is much more likely to contribute to low performance
    * Identifying key technological practices can drive high performance across your organization (and evidence that industry vertical doesn’t matter)

  speakers:
    - name: "Gene Kim"
      title: "CTO, Researcher and Author"
      about: "Gene Kim is a multiple award-winning CTO, researcher and author. He was founder and CTO of Tripwire for 13 years and is the co-author of the books The Phoenix Project: A Novel About IT, DevOps, and Helping Your Business Win, The DevOps Handbook, and Accelerate. Since 2014, he has been the organizer of the DevOps Enterprise Summit which studies the technology transformations of large, complex organizations."
      image: /images/webcast/gene-kim.jpg
    - name: "Ashish Kuthiala"
      title: "Director of Product Marketing at GitLab Inc."
      about: "Ashish Kuthiala is the Director of Product Marketing at GitLab Inc., a company based on the GitLab open-source project. Prior to working for GitLab, Ashish was Director of Global Strategy and Marketing for the Enterprise DevOps and Agile portfolio offerings at HP Software (acquired by Micro Focus). He has led teams across diverse functions including Marketing, Product Management, Business Development, Program management, R&D for building out and rolling enterprise grade software solutions for portfolio management and planning, application lifecycle management, testing, delivery, and IT Operations.."
      image: /images/webcast/ashish-kuthiala.png

- url: devops-speed-to-mission
  title: "DevOps: Powering your Speed to Mission"
  youtube_url: https://www.youtube.com/embed/E_TkejEMB8s
  subtitle: "A panel discussion on DevOps in the Public Sector"
  date: September 18th, 12pm ET / 4pm UTC
  form: 1592
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/webcast/public-sector.svg
  description: "Interested in learning what powerful impact DevOps is having on the Federal government and how DevOps can power your speed to mission? Join us for a webcast on Tuesday, September 18th at Noon Eastern to learn more."
  content: |

    Interested in learning what powerful impact DevOps is having on the Federal government and how DevOps can power your speed to mission? Join us for a webcast on Tuesday, September 18th at Noon Eastern to learn more.

    Join Tom Suder, President and Founder of ATARC, Leo Garciga, Chief of JD-OI6 and Chief Technology Officer for the JIDO/DTRA, Rob Brown, Division Chief and Senior Solutions Architect with Infrastructure Enterprise (EID) at USCIS, and John Jeremiah, Subject Matter Expert at GitLab for an interactive discussion.

    We will be taking questions at the end of the discussion, so come prepared to ask tough questions to the panelists.

    ### What we cover:
    * Cultural challenges faced by the government
    * Legal, regulatory and policy challenges faced by the government, and how to work within them
    * Best practices for successfully implementing a DevOps methodology
    * Tools and techniques which can be used within Federal agencies

  speakers:
    - name: "Tom Suder"
      title: "ATARC, Founder & President"
      about: "A respected thought leader in the Federal IT community, Tom Suder is the Founder and President of the Advanced Technology Academic Research Center (ATARC), a non-profit organization that provides a collaborative forum for Federal government, academia and industry to resolve emerging technology challenges."
      image: /images/webcast/tom-suder.jpg
    - name: "Leo Garciga"
      title: "Chief of JD-OI6 and Chief Technology Officer"
      about: "Leo Garciga serves as the Chief of JD-OI6 and Chief Technology Officer for the Joint Improvised-Threat Defeat Organization (JIDO), Defense Threat Reduction Agency (JIDO/DTRA). In these roles, he provides leadership and oversight of Mission Information Technology services and personnel that directly contribute to the implementation of the JIDO/DTRA mission and its support to the warfighter, Department of Defense (DoD), Combatant Commanders, Coalition partners, the intelligence and interagency organizations."
      image: /images/webcast/leo-garciga.png
    - name: "Rob Brown"
      title: "Chief Architect at the U.S. Citizenship and Immigration Services (USCIS)"
      about: "Rob Brown is the Chief Architect at the U.S. Citizenship and Immigration Services (USCIS). He is the technical lead for digitizing immigration and eProcessing. Rob has 18 years of experience in technical, managerial and business development solutions for the IT industry. Prior to USCIS, Rob was a Senior DevOps Engineer with Booz Allen Hamilton, responsible for leading the design, development, and implementation of Continuous Integration (CI) processes. He was also a member of the Agile Engineering and Test Automation Services (AETAS) Center of Excellence (COE). Rob holds a MS in Bioinformatics from George Mason University and a BS in Microbio/BioChem from Virginia Tech. He holds certifications from (ISC)², Sourcefire, Fonality, IC Agile and Juniper."
      image: /images/webcast/rob-brown.png
    - name: "John Jeremiah"
      title: "Enterprise DevOps evangelist and product marketing leader at GitLab"
      about: "John is an enterprise DevOps evangelist and product marketing leader at GitLab with over 20 years of IT leadership and software development experience. John helps to guide customers to adapt and thrive in a world of rapid change and digital disruption. He has held a variety of leadership roles with the U.S. Navy, IT consulting and Fortune 500 IT organizations and his experience spans from application developer, project and program manager, and IT Director where he has led the adoption of an Agile and CMMI Maturity Level 3 process framework. He has spoken at numerous conferences and events ranging from Interop to TedX. After receiving a bachelor’s degree from Oregon State University, Jeremiah earned his MS in Information Technology from The George Washington University School of Business and served nine years as a Naval Officer with the United States Navy."
      image: /images/team/johnjeremiah-crop.jpg

- url: gary-gruver-discussion
  youtube_url: https://www.youtube.com/embed/EqdAFtTngUg
  title: "DevOps Discussion with Gary Gruver"
  subtitle: "An insightful discussion on the details of a DevOps transformation"
  date: August 2, 9am PT / 4pm UTC
  form: 1592
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/webcast/devops-pipeline-bug.svg
  description: "Deciding to start a DevOps transformation is the first step in eliminating inefficiencies, but how do teams proceed?"
  content: |

    Deciding to start a DevOps transformation is the first step in eliminating inefficiencies, but how do teams proceed?

    Join Gary Gruver, author of Starting and Scaling DevOps in the Enterprise, as he shares his experience consulting with organizations to successfully adopt a DevOps model. He’ll offer advice on the influencers who should be involved in discussions and pinpoint the areas where a transformation should begin.

    ### What we cover:
    * The most common DevOps concerns
    * How enterprises can get started with DevOps
    * Best practices for successfully implementing a DevOps methodology
    * Key players in the transformation

  speakers:
    - name: "Gary Gruver"
      title: <a href="https://garygruver.com/" target="_blank">garygruver.com</a>
      about: "Gary Gruver is a consultant that spends his time running workshops with large organizations to help them transform their software development and delivery processes. He is the author of Starting and Scaling DevOps in the Enterprise and co-author of two other books: Leading the Transformation: Applying Agile and DevOps principles at Scale and A Practical Approach to Large Scale Agile Development: How HP Transformed LaserJet FutureSmart Firmware. He is also an experienced executive with a proven track record of transforming software development and delivery processes in large organizations. First as the R&D director of the LaserJet FW group that completely transformed how they developed embedded FW. Then as VP of QA, Release, and Operations at Macy’s.com leading the journey toward Continuous Delivery."
      image: /images/webcast/gary-gruver.jpg

- url: auto-devops
  youtube_url: https://www.youtube.com/embed/6LZQCCVGVDg
  title: "Release Radar: Auto DevOps"
  subtitle: "Accelerate delivery by 200% in two steps"
  date: June 27 9am PT / 4pm UTC
  form: 1592
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/concurrent-devops/concurrent-devops.svg
  description: "How many steps does it take to go from code to production? Automated pipelines are supposed to make software delivery faster and more efficient, but often require many integrations that need to be managed and maintained—making the process not so automatic."
  content: |

    How many steps does it take to go from code to production? Automated pipelines are supposed to make software delivery faster and more efficient, but often require many integrations that need to be managed and maintained—making the process not so automatic.

    However, with Auto DevOps, you can go from code to production in just two steps. Your pipeline is built into the same application as your repository, no integration necessary. Just write and commit your code, and Auto DevOps will do the rest: detect the language of your code and automatically build, test, measure code quality, scan for security issues, package, monitor, and deploy the application. Auto DevOps removes the barriers to shipping secure, bug-free code, fast.

    Join us for a live broadcast on June 27 to learn how Auto DevOps simplifies your deployment pipeline to accelerate delivery by 200%, improves responsiveness, and closes the feedback gap between you and your users.

    ### What we cover:
    * The competitive edge imperative: speed, feedback, and responsiveness.
    * What is Auto Devops and how does it work?
    * 2-step demo and pipeline interface walk through.

- url: starting-scaling-devops
  youtube_url: https://www.youtube.com/embed/aWlJGFkRcOA
  title: "Starting and Scaling DevOps"
  subtitle: "A tactical framework for implementing DevOps principles"
  date: June 19, 9am PT / 4pm UTC
  form: 1592
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/webcast/devops-pipeline-bug.svg
  description: "Companies worldwide are excited about DevOps and the many potential benefits of embarking on a DevOps transformation. The challenge, however, is figuring out where to begin and how to scale DevOps practices over time."
  content: |

    Companies worldwide are excited about DevOps and the many potential benefits of embarking on a DevOps transformation. The challenge, however, is figuring out where to begin and how to scale DevOps practices over time.

    Join Gary Gruver, author of Starting and Scaling DevOps in the Enterprise, as he covers how to analyze your current deployment pipeline to target your first improvements on the largest inefficiencies in software development and deployment. We’ll also explore the different approaches necessary for coordinating the work of small teams versus very large and complex organizations with many teams.

    ### What we cover:
    * Aligning your DevOps transformation to business objectives
    * The basics of a deployment pipeline
    * How to collect data and identify sources of inefficiency
    * Applying DevOps principles

  speakers:
    - name: "Gary Gruver"
      title: <a href="https://garygruver.com/" target="_blank">garygruver.com</a>
      about: "Gary Gruver is a consultant that spends his time running workshops with large organizations to help them transform their software development and delivery processes. He is the author of Starting and Scaling DevOps in the Enterprise and co-author of two other books: Leading the Transformation: Applying Agile and DevOps principles at Scale and A Practical Approach to Large Scale Agile Development: How HP Transformed LaserJet FutureSmart Firmware. He is also an experienced executive with a proven track record of transforming software development and delivery processes in large organizations. First as the R&D director of the LaserJet FW group that completely transformed how they developed embedded FW. Then as VP of QA, Release, and Operations at Macy’s.com leading the journey toward Continuous Delivery."
      image: /images/webcast/gary-gruver.jpg

- url: scalable-app-deploy
  youtube_url: https://www.youtube.com/embed/uWC2QKv15mk
  title: "Scalable app deployment"
  subtitle: "with GitLab and Google Cloud Platform"
  date: April 26th, 2 PM EDT
  form: 1554
  success_message: "Thank you for registering. You will receive an email with additional webcast details."
  image: /images/webcast/gitlab-gke-integration.svg
  description: "With the GitLab + Google Kubernetes Engine (GKE) integration, developers have the power to spin up a Kubernetes cluster managed by Google Cloud Platform (GCP) in a few clicks. The integration’s versatility speeds up software development and delivery while maintaining security and scale, allowing developers to focus on building apps instead of managing infrastructure."
  content: |

    With the GitLab + Google Kubernetes Engine (GKE) integration, developers have the power to spin up a Kubernetes cluster managed by Google Cloud Platform (GCP) in a few clicks. The integration’s versatility speeds up software development and delivery while maintaining security and scale, allowing developers to focus on building apps instead of managing infrastructure.

    Join William Chia, Senior Product Marketing Manager at GitLab, and William Denniss, Product Manager at Google, as they explain how to deploy applications at scale using GKE and GitLab’s robust Auto DevOps capabilities.

    ### What we cover:
    * The basics of cloud environments, including benefits and challenges
    * How the GitLab GKE integration simplifies setting up and deploying to a Kubernetes cluster
    * A demo highlighting how easy it is to set up a Kubernetes Cluster, how to deploy your app using GitLab CI/CD, and how GKE allows you to deploy at scale
