require 'yaml'
require_relative './team/member'

module Gitlab
  module Homepage
    class Team
      def members
        @members ||= Team::Member.all! do |member|
          Team::Project.all! do |project|
            member.assign(project) if member.involved?(project)
          end
        end
      end

      def members_with_invalid_start_dates
        Team::Member.all!.reject do |member|
          member.start_date.nil? || member.start_date.is_a?(Date)
        end
      end

      def projects
        @projects ||= Team::Project.all! do |project|
          Team::Member.all! do |member|
            project.assign(member) if member.involved?(project)
          end
        end
      end

      def departments
        @departments ||= begin
          Hash.new(0).tap do |departments|
            Team::Member.all! do |member|
              member.departments.each { |department| departments[department] += 1 }
            end
          end.sort
        end
      end

      def countries
        @countries ||= begin
          members_by_country = Team::Member.all!.group_by(&:country)

          members_by_country.delete('Remote')
          members_by_country.delete(nil)

          members_by_country
            .map { |name, members| { name: name, count: members.count, info: members.first.country_info } }
            .sort_by { |c| c[:info]&.name || c[:name] }
        end
      end

      def direct_team(manager_role:, role_regexp: nil)
        manager = Team::Member.all!.find { |member| member.text_role == manager_role }

        return [] unless manager

        [manager] +
          Team::Member.no_vacancies.select { |member| member.reports_to == manager.slug && (role_regexp.nil? || member.text_role =~ role_regexp) }
      end

      def role_matches(role_regexp:)
        Team::Member.no_vacancies.select { |member| member.text_role =~ role_regexp }
      end

      def department_matches(department:)
        Team::Member.all!.select { |member| member.departments.include? department }
      end
    end
  end
end
