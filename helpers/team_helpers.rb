module TeamHelpers
  def direct_team(manager_role:, role_regexp: nil)
    members = Gitlab::Homepage::Team.new.direct_team(manager_role: manager_role, role_regexp: role_regexp)

    partial('includes/team_member_table', locals: { team_members: members })
  end

  # Stable counterparts for a team, found by matching roles against
  # role_regexp. Anyone in the direct team reporting to direct_manager_role will
  # be excluded.
  def stable_counterparts(role_regexp:, direct_manager_role:)
    members =
      Gitlab::Homepage::Team.new.role_matches(role_regexp: role_regexp) -
      Gitlab::Homepage::Team.new.direct_team(manager_role: direct_manager_role)

    partial('includes/team_member_table', locals: { team_members: members })
  end

  def hiring_chart(department: nil)
    members = Gitlab::Homepage::Team.new
      .department_matches(department: department)
      .sort_by(&:start_date)

    person_count = vacancy_count = 0
    today_str = Date.today.strftime '%Y-%m-%d'
    people = {}
    people_not_started_yet = []
    vacancies = {}
    vacancies_overdue = []

    members.select { |member| member.type == 'person' }.each do |person|
      person_count += 1
      if person.start_date.to_date > Date.today
        date_str = today_str
        people_not_started_yet.push(person)
      else
        date_str = person.start_date
      end
      people[date_str] = person_count
    end
    people[today_str] = person_count

    vacancy_count = person_count
    members.select { |member| member.type == 'vacancy' }.each do |vacancy|
      vacancy_count += 1
      if date_str = vacancy.start_date.to_date > Date.today
        date_str = vacancy.start_date
      else
        date_str = today_str
        vacancies_overdue.push(vacancy)
      end
      vacancies[date_str] = vacancy_count
    end
    vacancies[today_str] = person_count

    partial('includes/hiring-chart.html.erb', locals: { department: department, people: people, vacancies: vacancies, people_not_started_yet: people_not_started_yet, vacancies_overdue: vacancies_overdue })
  end
end
