---
layout: markdown_page
title: "Roles and Personas"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Roles vs personas

Personas describe the ideal target for GitLab. They help us define our messaging and marketing delivery. They are theoretical people to target. By defining their concerns and where they go for information, we can best spend our marketing dollars and sales efforts by focusing on this ideal target.  

Roles are distinct job titles. These are the real people you will encounter while selling. You will find a contact at an account with a specific role. Understanding the challenges faced by each role in IT, along with what they care most about, is helpful to  deliver the right value proposition to the right person.

## Personas

Personas are a generalized way of talking about the ideal target we are aiming to communicate with and design for. They help us not only define our messaging and marketing delivery, but also our product. Keeping personas in mind allows us to use the correct language and make the best decisions to address their specific problems and pain points. GitLab has both [buyer](#buyer-personas) and [user](#user-personas) persona types.

### Buyer personas

Buyer personas are the people who serve as the main buyer in an organization or
a champion within an enterprise the drives the buying conversation and
coordinates various teams to make a purchase.

1. Director DevOps [video ](https://www.youtube.com/watch?v=qyELotxsQzY), Director DevOps [slide deck](https://docs.google.com/presentation/d/1x-XUhAXkZxl8ZGe4ze9qcWMmrWITc7es1fYNY_OHQQA/) or Director of IT
1. VP IT [video](https://www.youtube.com/watch?v=LUh5eevH3F4), VP IT [Slide Deck](https://docs.google.com/presentation/d/17Ucpgxzt1jSCs83ER4-LdDyEuermpDuriugPNYrz8Rg/) or VP of Engineering
1. Chief Architect [Video](https://www.youtube.com/watch?v=qyELotxsQzY), Chief Architect [slide deck](https://docs.google.com/presentation/d/1KXsozYkimSLlEg3N-sKeN7Muatz_4XimYp-s7_dY1ZM/) or CIO
1. Chief Information Security Officer or Director of Security (may fund Ultimate upgrade for security capabilities)

Also see [the four tiers on our pricing page](https://about.gitlab.com/handbook/ceo/pricing/#four-tiers).

#### Enterprise IT roles we sell to

While personas describe ideal targets, roles are the real people in job titles you will encounter while selling. Understanding the challenges faced by each role in IT, along with what they care most about, is helpful to  deliver the right value proposition to the right person. It will help you know:   
    a) if they are even someone who may have an interest in GitLab (don't waste time),  
    b) what questions to ask to learn more and qualify the lead,  
    c) what value prop they'll want to hear that could get them to a demo, discussion, or POC.  

See the [Enterprise IT Roles page](https://about.gitlab.com/handbook/marketing/product-marketing/enterprise-it-roles/) for more details about who we sell to and how to approach them.

#### Writing for all personas
- When writing content, always remember to use the [GitLab voice](https://gitlab.com/gitlab-com/marketing/general/blob/master/content/editorial-style-guide.md) regardless of the persona.
- Never alienate other personas. You can appeal to a buyer without buzzword bingo.
- Look at who uses a channel most (twitter & docs: user, webinar linkedin: buyer) when shared (website) guide them /features for users /solutions for buyers.

### User personas
User personas are people who actually use GitLab. They may or may not be the person in
the organization who has the authority and budget to purchase Gitlab, but they
are heavy influencers in the buying process.

#### Parker (Product Manager)

* **Alternative Job Titles:** Program Manager, Project Manager, Technical Product Manager, Head of Product

##### Job Summary

I am responsible for defining and scoping features, incorporating company objectives into the product roadmap, and giving developers and designers the requirements they need to deliver strong features. Whether I’m making sure I have the right skill-sets on the team, prioritizing feature requests, or ensuring that we deliver on time, my job is to set my team up for success.

##### Motivations

* When company objectives shift, I want to have a standard process for communication in place, so that I can be in sync with all team members.
* When development begins, I want to see an overview of all the relevant information related to a feature or project, so that I can monitor progress throughout a cycle.
* When I’m planning for the next cycle, I want to see a history of how accurately the developers on my team estimated their capacity, so that I can be sure that key features will be delivered on time.

##### Frustrations

* It can be difficult to know the status of certain requirements, when my team members do not take the time to update the various tools we use.
* I often have trouble balancing feature requests with capacity.
* It can be difficult to give clients a reasonable timeframe that is not off-base, since a cycle is often unpredictable.
* It’s challenging to explain why certain features have been delayed or deprioritized, when customers and upper-level management are not working closely with the team.

#### Delaney (Development Team Lead)

* **Alternative Job Titles:** Technical Manager, Software Engineering Team Lead, Technical Team Lead, Software Development Director, Development Lead

##### Job Summary

I am responsible for meeting with the product management team to discuss and schedule features, so we can convert concepts into practical solutions. I ensure that capacity is properly estimated, create program specifications, and often mentor junior developers.

##### Motivations

* When discussing feature requests, I want to receive clear requirements from the product and design teams, so my team can deliver on time and reduce back-and-forth communication.
* When assessing team resources, I want to see a history of how accurately developers on my team have estimated their capacity, so that I can assign them to the right tasks.
* When important deadlines are approaching, I want all team members to reliably update our tools, so that I can track progress without having to search for relevant information in other communication channels.

##### Frustrations

* It can be difficult for my team to do thorough code reviews in a reasonable timeframe, while still making progress on their own assignments.
* When demand surpasses our current capacity, it can be stressful to resolve issues while not creating new ones that result from hasty work.
* I am not always aware of the best way to explain technical limitations to stakeholders who are not involved in the development process.

#### Sasha (Software Developer)

* **Alternative Job Titles:** Software Engineer, Application Developer, Digital Solutions Developer, Consultant, Database Developer, Mobile Developer

##### Job Summary

I spend the majority of my time focused on completing planned development tasks, with roughly 30-40% of time taken by meetings, planning for the next sprint, and fixing bugs or customer requests as they arise. I work off of JIRA tickets and have a regular stand-up with my team.

##### Motivations

* When I’m planning work, I want to have better communication between stakeholders, so I can deliver something they really need and use.
* When I’m on-call, I want to be the expert on some part of the system, so I know that I’m a valuable part of the team.
* When collaborating with a large number of developers, I want to see a record of everyone’s changes, so we can pinpoint and unwind mistakes.
* When I’m pairing with my teammates, I want to learn new tools and skills, so I can keep growing in my career.

##### Frustrations

* I’m frustrated when requirements change after work has already begun on a project.
* I’m frustrated when work is inaccurately scoped, because it causes stress and eats into time planned for other work.
* I’m frustrated when I come across brittle code and something that should be an easy fix requires a lot of rework.
* I’m concerned that by taking longer than expected on a task I may be judged or seen as blocking others’ work.

#### Devon (DevOps Engineer)

* **Alternative Job Titles:** Application Developer, Operations Engineer, Systems Engineer, IT Consultant

##### Job Summary

I provide support for our infrastructure, environments, and integrations. I split my time between coding to implement features and bug fixes, and helping developers deploy, build, and release as efficiently as possible.

##### Motivations

* When developers plan a project that requires my support, I want to be notified, so I can avoid unnecessary reactive work.
* When I resolve problems, I want to be able to track impact and other important success metrics, so I can raise my profile in the organization.
* When I create a solution for developers, I want to see it being used, so I know that my contributions are reliable and valued.
* When I’m facing a new or novel challenge, I want to whiteboard it with a teammate, so I have the satisfaction of helping solve a problem that has no manual.
* When I’m advocating for a new process or tool, I want to point to a metric or test, so I can support my case with something objective.

##### Frustrations

* It is hard for me to quantify my efforts and convince others of time that will be saved in the future by investing time in proactive tasks in the present.
* I'm frustrated by the avoidable crises, caused by a lack of communication, that derail my work and burn up my resources.
* I dislike frequent context-switching and being responsible for some tasks that I feel I am not good at, because of the many hats my job requires me to wear.
* I'm frustrated by the politics of convincing people to adopt my recommendations.

#### Sidney (Systems Administrator)

* **Alternative Job Titles:** Systems Engineer, Database Administrator, Infrastructure Engineer, Site Availability Engineer, Site Reliability Engineer

##### Job Summary

I maintain and scale our infrastructure and configurations, and my priority is to automate as much as possible. When needed, I also build servers and help developers deploy to them.

##### Motivations

* When I get asked for help multiple times on the same task, I want to automate that task, to save time and avoid mistakes in the future.
* When I’m on-call, I want to receive tiered notifications, so that the true emergencies don’t get lost in the noise.
* When I’m releasing an improvement, I want no one to notice, so I know that it has gone smoothly.

##### Frustrations

* I'm frustrated by the large volume of reactive work that I face.
* I'm frustrated by the number of channels (email, Skype, SMS, Slack, pager) on which I receive on-call notifications.
* I'm frustrated when I get inundated by requests from people who have not followed the correct process.
* I'm frustrated when developers do not implement my recommendations, and I’m responsible for fixing their preventable problems anyway.

#### Sam (Security Analyst)

* **Alternative Job Titles:** Security Engineer, Security Consultant, or an Application Security Specialist

##### Job Summary

I wear lots of hats, but the majority of my time is spent monitoring and flagging events, running down high priority tasks and working with other teams to implement new systems.

##### Motivations

* When I’m monitoring my dashboards, I want to see everything I am monitoring in one tool, so I can do my job easier and more efficiently.
* When security testing, I want to be more proactive than reactive, so I can anticipate potential threats or vulnerabilities before the bad guys do.
* When on the job, I want to stay up to date on the latest information and education in information security, so I can grow in my career.

##### Frustrations

* I’m frustrated I don’t have the resources to complete this project to its specifications.
* I’m frustrated when I know how to fix a security issue but the red tape at my company doesn’t allow me to in a timely manner.
* I’m concerned that what I don’t know, I don’t know and my company can be attacked in any manner of ways at any time.
* I’m concerned that I might miss something and my company may become compromised because of it.
