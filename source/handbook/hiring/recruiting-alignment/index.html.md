---
layout: markdown_page
title: "Recruiting Alignment"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiter and Coordinator Alignment by Hiring Manager

| Sales                    | Recruiter       | Coordinator     |
|--------------------------|-----------------|-----------------|
| Michael McBride          | Kelly Murdock   | April Hoffbauer |
| Francis Aquino           | Kelly Murdock   | April Hoffbauer |
| Paul Almeida             | Kelly Murdock   | April Hoffbauer |
| Kristen Lawrence         | Kelly Murdock   | April Hoffbauer |
| Richard Pidgeon          | Nadia Vatalidis | Kike Adio       |
| Michael Alessio          | Nadia Vatalidis | Kike Adio       |

| Marketing                                      | Recruiter       | Coordinator     |
|------------------------------------------------|-----------------|-----------------|
| Erica Lindberg                                 | Jacie Zoerb     | April Hoffbauer |
| Ashish Kuthiala                                | Jacie Zoerb     | April Hoffbauer |
| LJ Banks                                       | Jacie Zoerb     | April Hoffbauer |
| Alex Turner                                    | Jacie Zoerb     | April Hoffbauer |
| Director of Field Marketing (to be hired, Leslie Blanchard as interim) | Jacie Zoerb     | April Hoffbauer |
| Director of Marketing Operations (to be hired, LJ Banks as interim) | Jacie Zoerb     | April Hoffbauer |
| David Planella                                 | Jacie Zoerb     | April Hoffbauer |
| Melissa Smolensky                              | Jacie Zoerb     | April Hoffbauer |
| Elsje Smart                                    | Nadia Vatalidis | Kike Adio |

| Engineering           | Recruiter                                               | Coordinator |
|-----------------------|---------------------------------------------------------|-------------|
| Frontend              | Eva Petreska                                            | Emily Mowry |
| Quality               | Eva Petreska                                            | Emily Mowry |
| UX                    | Eva Petreska                                            | Emily Mowry |
| Support               | Nadia Vatalidis                                         | Kike Adio   |
| Infrastructure        | Matt Allen                                              | Emily Mowry |
| Security              | Steve Pestorich                                         | Emily Mowry |
| Backend - Dev         | Liam McNally                                            | Kike Adio   |
| Backend - Secure      | Liam McNally                                            | Kike Adio   |
| Backend - Ops         | Trust Ogor                                              | Kike Adio   |
| Backend - Enablement  | Trust Ogor                                              | Kike Adio   |

| Product           | Recruiter                       | Coordinator |
|-------------------|---------------------------------|-------------|
| Mark Pundsack     | Matt Allen                      | Emily Mowry |

| Other         | Recruiter                       | Coordinator               |
|---------------|---------------------------------|---------------------------|
| Paul Machle   | Jacie Zoerb/Nadia Vatalidis     | April Hoffbauer/Kike Adio |
| Carol Teskey  | Jacie Zoerb/Kelly Murdock       | April Hoffbauer           |
| Brandon Jung  | Kelly Murdock                   | April Hoffbauer           |
| Meltano       | Steve Pestorich                 | Emily Mowry               |

## Sourcer Alignment by Division and Location

| Product               | Region            | Sourcer                | Estimated % |
|-----------------------|-------------------|------------------------|-------------|
| Sales & Marketing     | Americas/Anywhere | Stephanie Garza        | 100%        |
| Sales & Marketing     | EMEA & APAC       | Anastasia Pshegodskaya | 25%         |
| Engineering Backend   | Anywhere          | Zsuzsanna Kovacs       | 100%        |
| Engineering General   | Anywhere          | Anastasia Pshegodskaya | 50%         |
| Other/Director+ Roles | Anywhere          | Anastasia Pshegodskaya | 25%         |
