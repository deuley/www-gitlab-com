---
layout: markdown_page
title: "Data Team"
description: "GitLab Data Team Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----

## <i class="fab fa-gitlab fa-fw icon-color font-awesome" aria-hidden="true"></i> Quick Links
[Primary Project](https://gitlab.com/gitlab-data/analytics/){:.btn .btn-purple-inv}

[dbt docs](https://gitlab-data.gitlab.io/analytics/dbt/snowflake/#!/overview){:.btn .btn-purple-inv}

## <i class="fas fa-map-signs fa-fw icon-color font-awesome" aria-hidden="true"></i> Roadmap

[Epics](https://gitlab.com/groups/gitlab-data/-/roadmap?layout=MONTHS&sort=start_date_asc){:.btn .btn-purple}

[OKRs](https://about.gitlab.com/company/okrs/){:.btn .btn-purple}

----

<br />

<div class="alert alert-purple center">
  <p class="purple center" style="font-size: 34px ; text-align: center ; margin: auto">
    <strong>We <i class="fab fa-gitlab orange font-awesome" aria-hidden="true"></i> Data</strong>
  </p>
</div>
{:.no_toc}

## <i class="fas fa-chart-line fa-fw icon-color font-awesome" aria-hidden="true"></i> Data Analysis Process

Analysis usually begins with a question. A stakeholder will ask a question of the data team by creating an issue in the [Data Team project](https://gitlab.com/gitlab-data/analytics/) using the appropriate template. The analyst assigned to the project may schedule a discussion with the stakeholder(s) to further understand the needs of the analysis. This meeting will allow for analysts to understand the overall goals of the analysis, not just the singular question being asked, and should be recorded. Analysts looking for some place to start the discussion can start by asking: 
* How can your favorite reports be improved?
* How do you use this data to make decisions?
* What decisions do you make and what information will help you to make them quicker/better?

An analyst will then update the issue to reflect their understanding of the project at hand. This may mean turning an existing issue into a meta issue or an epic. Stakeholders are encouraged to engage on the appropriate issues. The issue then becomes the SSOT for the status of the project, indicating the milestone to which its been assigned and the analyst working on it, among other things. Barring any confidentiality concerns, the issue is also where the final project will be delivered. On delivery, the data team  manager will be cc'ed where s/he will provide feedback and/or request changes. When satisfied, s/he will close the issue. If the stakeholder would like to request a change after the issue has been closed, s/he should create a new issue and link to the closed issue. 

The Data Team can be found in the #analytics channel on slack.

## <i class="fas fa-tasks fa-fw icon-color font-awesome" aria-hidden="true"></i> Getting Things Done

The data team currently works in two-week intervals, called milestones. Milestones start on Tuesdays and end on Mondays. This discourages last-minute merging on Fridays and allows the team to have milestone planning meetings at the top of the milestone.

Milestones may be three weeks long if they cover a major holiday or if the majority of the team is on vacation. As work is assigned to a person and a milestone, it gets a weight assigned to it. 

### Issue Pointing

* Refer to the table below for point values and what they represent. 
* We size and point issues as a group. Size is about the complexity of the problem and not its complexity in relation to whom is expected to complete the task.
* Effective pointing requires more fleshed out issues, but that requirement shouldn't keep people from creating issues.
* When pointing work that happens outside of the Data Team projects, add points to the issue in the relevant Data Team project and ensure issues are cross-linked.

| Weight | Description |
| ------ | ------ |
| Null | Meta, Discussion, or Documentation issues that don't result in an MR |
| 0 | Should not be used. |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. | 
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. | 
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. | 
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. | 
| 13 | A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues. | 

### Issue Labeling

Think of each of these groups of labels as ways of bucketing the work done. All issues should get the following classes of labels assigned to them:

* Who (Purple): Team for which work is primarily for (Data, Finance, Sales, etc.)
* What - Data or Tool
  * Data (Light Green): Data being touched (Salesforce, Zuora, Zendesk, Gitlab.com, etc.)
  * Tool (Light Blue) (Looker, dbt, Stitch, Airflow, etc.) 
* Where (Brown): Stage of data lifecycle (Infrastructure, Extract/Load, Transform, Reporting)
* When (Varies): Priority (P1-4 See table below, Backlog, 2019-Q3, etc.) 
* How (Orange): Type of work (Documentation, Break-fix, Enhancement, Refactor, Testing, Housekeeping)

Optional labels that are useful to communicate state or other priority
* State (Red) (Won't Do, Blocked, Needs Consensus, etc.)


| Priority | Description | Probability of shipping in milestone | 
| ------ | ------ | ------ |
| P1 | **Urgent**: top priority for achieving in the given milestone. These issues are the most important goals for a milestone and should be worked on first; some may be time-critical or unblock dependencies. | ~100% |
| P2 | **High**: important issues that have significant positive impact to the business or technical debt. Important, but not time-critical or blocking others.  | ~75% |
| P3 | **Normal**: incremental improvements to existing features. These are important iterations, but deemed non-critical. | ~50% |
| P4 | **Low**: stretch issues that are acceptable to postpone into a future milestone. | ~25% | 


### Merge Request Workflow

*Ideally*, your workflow should be as follows:
1. Create an issue or open an existing issue.
1. Add appropriate labels to the issue issue (see above)
1. Open an MR from the issue using the "Create merge request" button. This automatically creates a unique branch based on the issue name. This marks the issue for closure once the MR is merged.
1. Push your work to the branch
1. Run any relevant jobs to the work being proposed
  * e.g. if you're working on dbt changes, run the dbt MR job and the dbt test job.
1. Document in the MR description what the purpose of the MR is, any additional changes that need to happen for the MR to be valid, and if it's a complicated MR, how you verified that the change works. See [this MR](https://gitlab.com/gitlab-data/analytics/merge_requests/658) for an example of good documentation. The goal is to make it easier for reviewers to understand what the MR is doing so it's as easy as possible to review.
1. Assign the MR to a peer to have it reviewed. If assigning to someone who can merge, either leave a comment asking for a review without merge, or you can simply leave the `WIP:` label.
1. Once it's ready for further review and merging, remove the `WIP:` label, mark the branch for deletion, mark squash commits, and assign to the project's maintainer. Ensure that the attached issue is appropriately labeled and pointed.

Other tips:
* If, for some reason, the merge request is closed but not Merged, you have to run the review stop job manually. Closing the MR will not trigger it. 
* If you're on a review instance of the database and you need to test a change to the SFDC snapshot, truncate the table first.


## <i class="fas fa-exchange-alt fa-fw icon-color font-awesome" aria-hidden="true"></i> Extract and Load

We currently use Stitch for most of our data sources. In some cases, we use [Meltano](/handbook/meltano/).

| Data Source       | Pipeline  | Management Responsibility                                                                                                                                              |
|-------------------|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| CloudSQL Postgres | Stitch    | Data Team                                                                                                                                                              |
| Gitter            |           |                                                                                                                                                                        |
| SheetLoad         | SheetLoad | Data Team                                                                                                                                                              |
| Marketo           | Stitch    | Data Team                                                                                                                                                              |
| Netsuite          | Stitch    | Data Team                                                                                                                                                              |
| Pings             | Meltano   | The Meltano team will investigate the source of the problem.  If the issue is in the Meltano tap, the Meltano team will fix it.  If not, the data team is responsible. |
| SFDC              | Stitch    | Data Team                                                                                                                                                              |
| Snowplow          |           | Data Team                                                                                                                                                              |
| Zendesk           | Stitch    | Data Team                                                                                                                                                              |
| Zuora             | Stitch    | Data Team                                                                                                                                                              |

### Adding new Data Sources

Process for adding a new data source:
* Create a new issue in the Analytics repo requesting for the data source to be added:
  * Document what tables and fields are required
  * Document the questions that this data will help answer
* Create an issue in the [Security project](https://gitlab.com/gitlab-com/gl-security/engineering/issues/) and cross-link to the Analytics issue.
  * Tag the Security team `gitlab-com/gl-security`


### Using SheetLoad

SheetLoad is the process by which a GoogleSheet, local CSV or file from GCS can be ingested into the data warehouse. This is not an ideal solution to get data into the warehouse, but may be the appropriate solution at times. 

As it is being iterated on often, the best place for up-to-date info on Sheetload is the [Sheetload readme](https://gitlab.com/gitlab-data/analytics/tree/master/extract/sheetload).






## <i class="fas fa-clock fa-fw icon-color font-awesome" aria-hidden="true"></i> Orchestration

We are in the process of moving from GitLab CI to Airflow.

## <i class="fas fa-database fa-fw icon-color font-awesome" aria-hidden="true"></i> Data Warehouse

We currently use [Snowflake](https://docs.snowflake.net/manuals/index.html) as our data warehouse.

### Warehouse Access

To gain access to the data warehouse: 
* Create an issue in the [access requests project](https://gitlab.com/gitlab-com/access-requests) documenting the level of access required.
* Do not request a shared account - each account must be tied to a user.

<div class="panel panel-success">
**Managing Roles for Snowflake**
{: .panel-heading}
<div class="panel-body">

Here are the proper steps for provisioning a new user and user role:

* Login and switch to `SECURITYADMIN` role
* Create user (`EBURKE`)
* Create a password using https://passwordsgenerator.net/
* Click next and fill in additional info. 
  * Make Login Name and Display name match user name (all caps).
  * Do not set any defaults.
  * Send to person using https://onetimesecret.com/
* Create role for user (`EBURKE` for example) with `SYSADMIN` as the parent role (this grants the role to sysadmin)
* Grant user role to new user
* Create user_scratch schema in `ANALYTICS` as `SYSADMIN`
  * `CREATE SCHEMA eburke_scratch;`
* Grant ownership of scratch schema to user role
  * `GRANT OWNERSHIP ON schema eburke_scratch TO ROLE eburke;`
* Document in Snowflake config.yml permissions file
</div>
</div>


## <i class="fas fa-cogs fa-fw icon-color font-awesome" aria-hidden="true"></i> Transformation

* Getting started with dbt [Coming soon]
* How to use dbt docs [Coming soon]

### Tips and Tricks about Working with dbt
* The goal of a (final) `_xf` dbt model should be a `BEAM*` table, which means it follows the business event analysis & model structure and answer the who, what, where, when, how many, why, and how question combinations that measure the business. 
* Model names should be as obvious as possible and should use full words where possible, e.g. `accounts` instead of `accts`.
* Documenting and testing new data models is a part of the process of creating them. A new dbt model is not complete without tests and documentation.  
* Definitions to know
   * `source table` - (can also be called `raw table`) table coming directly from data source as configured by the manifest. It is stored directly in a schema that indicates its original data source, e.g. `sfdc`
   * `base models`- the only dbt models that reference the source table; base models have minimal transformational logic (usually limited to filtering out rows with data integrity issues or actively flagged not for analysis and renaming columns for easier analysis); can be found in `analytics` schema; is used in `ref` statements by `end-user models`
   * `end-user models` - dbt models used for analysis. The final version of a model will likely be indicated with an `_xf` suffix when it’s goal is to be a `BEAM*` table. It should follow the business event analysis & model structure and answer the who, what, where, when, how many, why, and how question combinations that measure the business.


## <i class="fas fa-chart-bar fa-fw icon-color font-awesome" aria-hidden="true"></i> Visualization

We are currently evaluating multiple visualization tools.

## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> Team Roles

### Data Analyst
[Position Description](/job-families/finance/data-analyst/){:.btn .btn-purple}

### Data Engineer
[Position Description](/job-families/finance/data-engineer/){:.btn .btn-purple}

### Manager
[Position Description](/job-families/finance/manager-data-and-analytics/){:.btn .btn-purple}


<!-- EXTRA STYLES APPLIED FOR THIS PAGE ONLY -->

<style>
.purple {
  color: rgb(107,79,187) !important;
}
.orange {
  color:rgb(252,109,38) !important;
}
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
.btn-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-purple:hover {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv:hover {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-orange {
  color: rgb(252,109,38);
  background-color: #fff;
  border-color: rgb(226,67,41);
}
.btn-orange:hover {
  color: #fff;
  background-color: rgb(252,109,38);
  border-color: rgb(226,67,41);
}
.product.thumbnail img {
  display: block;
  max-width: 50%;
  margin: 20px auto;
}
.thumbnail img {
  display: block;
  max-width: 30%;
  margin: 20px auto;
}
.caption h4 {
  text-align: center;
}
.mkt-box {
  padding-bottom: 10px;
  padding-top: 10px;
  cursor: pointer;
}
.mkt-box:hover {
  /*border-radius: 5px;*/
  box-shadow:0 1px 5px rgba(0,0,0,0.3), 0 0 2px rgba(0,0,0,0.1) inset;
}
.mkt-row {
  padding-top: 20px;
  padding-bottom: 5px;
}
.mkt-row a:focus {
  outline: none;
}
.modal-header h2 {
  margin-top: 0;
}
.modal-footer p {
  margin-bottom: 0;
}
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.description {
  color: #999;
}
.extra-space {
  margin-bottom: 5px;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
ul.toc-list-icons li ul li i.slack {
  color: rgb(224,23,101);
}
ul.toc-list-icons li ul li i.email {
  color: rgb(192,0,0);
}
</style>

