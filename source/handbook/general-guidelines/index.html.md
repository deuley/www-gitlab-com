---
layout: markdown_page
title: "GitLab General Guidelines"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overall
1. Working at GitLab Inc. is cooperating with the most talented people you've ever worked with, being the **most productive** you'll ever be, and creating software that is helping the most people you've ever reached.
1. We recognize that inspiration is perishable, so if you’re **enthusiastic** about something that generates great results in relatively little time feel free to work on that.
1. Do what is right for our **customers** and the rest of the GitLab community, do what is best over the long term, don't be evil.
1. We create **simple** software to accomplish complex collaborative tasks.
1. Please read and contribute to our [**strategy**](/company/strategy/).
1. Also see our [handbook usage](/handbook/handbook-usage/) and [leadership guidelines](/handbook/leadership/#guidelines).
1. We want to have a great company so if you meet people that are **better** than yourself try to recruit them to join the company.


## Getting things done
1. We **use** our own software to accomplish complex collaborative tasks.
1. We take ownership and start by creating a merge request. If you see something that needs to be improved submit a merge request. Don't tell someone else about the problem and expect them to start the merge request. "If you see something don't say something, if you see something create an MR."
1. For each action or comment, it helps to ask yourself (i) does this help the company achieve its strategic goals? (ii) is this in the company's interest, and finally, (iii) how can I contribute to this effort/issue in a constructive way?
1. There is no need for **consensus**, make sure that you give people that might have good insights a chance to respond (by /cc'ing them) but make a call yourself because [consensus doesn't scale](https://twitter.com/sama/status/585950222703992833).
1. Everyone at the company cares about your **output**. Being away from the keyboard during the workday, doing private browsing or making personal phone calls is fine and encouraged.
1. Explicitly note what **next action** you propose or expect and from whom.
1. Before **replying** to a request, complete the requested task first. Otherwise, indicate when you plan to complete it in your response. In the latter case, always send a message after the task is subsequently completed.
1. If you don't have time to do something let people know when they give you the tasks instead of having it linger so they can find an alternative. You can use the text: "I have other priorities and can't help with this" or "I can complete this on May 25, please let me know if that is OK".


### Write it down
1. {: #handbook-first} Develop procedures and templates in a **[handbook-first](../handbook-usage/#why-handbook-first)** way, as opposed to migrating content to the handbook later from Google/Word documents.
   - This ensures the handbook is always up-to-date.
   - This makes them easier to find and suggest changes to with the organization and shows our commitment to open collaboration outside the organization.
   - This means discussion and collaboration on this content should happen in issue comments or merge request comments.
1. When creating any content, create it **web-first**, as opposed to using Google slides, sheets, docs, etc, because they can only be found and contributed to by team members, and not by Users, Customers, Advocates, Google Handbook searches, or Developers.
1. If you fix something for GitLab.com or one of our users, make sure to make that the default (preferred) and **radiate** the information in the docs. We should run GitLab.com with the same default settings and setup our users would also have, if we deviate from it we should add it to the [settings page for GitLab.com](/gitlab-com/settings/).
1. Everything is **always in draft** and subject to change, including this handbook. So do not delay documenting things and do not include "draft" in the titles of documents. Ensure everyone can read the current state. Nothing will ever be finished.


## Behavior and language
1. Do **not** make jokes or unfriendly remarks about race, ethnic origin, skin color, gender, or sexual orientation.
1. Use **inclusive** language. For example, prefer "Hi everybody" or "Hi people" to "Hi guys". While there are several good guides from folks like [18f](https://content-guide.18f.gov/inclusive-language/), [University of Calgary](https://www.ucalgary.ca/odepd/files/odepd/inclusive-language-guide-june-13-2017.pdf), and [Buffer](https://open.buffer.com/inclusive-language-tech/) on using inclusive language, we don't keep an exhaustive list. When new possibly non-inclusive words arise, we prefer to be proactive and look for an alternative. If your goal is to be inclusive, it is more effective to make a small adjustment in the vocabulary when some people have a problem with it, rather than making a decision to not change it because some people don’t think it is a problem.
1. Share problems you run into, ask for help, be forthcoming with information and **speak up**.
1. Don't display surprise when people say they don't know something, as it is important that everyone feels comfortable saying "I don't know" and "I don't understand." (As inspired by [Recurse](https://www.recurse.com/manual#sub-sec-social-rules).)
1. You can always **refuse** to deal with people who treat you badly and get out of situations that make you feel uncomfortable.
1. Everyone can **remind** anyone in the company about these guidelines. If there is a disagreement about the interpretations, the discussion can be escalated to more people within the company without repercussions.
1. If you are unhappy with anything (your duties, your colleague, your boss, your salary, your location, your computer) please let your boss, or the CEO, know as soon as you realize it. We want to solve problems while they are **small**.
1. Make a conscious effort to **recognize** the constraints of others within the team. For example, sales is hard because you are dependent on another organization, and Development is hard because you have to preserve the ability to quickly improve the product in the future.

## Not public

We make public by default because [transparency is one of our values](/handbook/values/#transparency).
However it is [most important to focus on results](/handbook/values/#hierarchy).
So 
Most things are **public** unless there is a reason not to. Not public by default are the following items:

1. Security vulnerabilities are not public since it would allow attackers to compromise GitLab installations. We do make them public after we remediated a vulnerability.
1. Financial information is confidential because we don't have audited financials yet.
1. Legal discussions are not public due to the [purpose of Attorney-Client Privilege](/handbook/legal/#what-is-the-purpose-of-these-privileges).
1. Individual job applications, compensation, and feedback are not public since it can contain negative feedback and [negative feedback is 1-1](/handbook/values/#collaboration) between you and your manager. [Job families](https://about.gitlab.com/handbook/hiring/job-families/) and our [Compensation Calculator](/handbook/people-operations/global-compensation/calculator/) are public.
1. Partnerships with other companies are not public since the partners are frequently not comfortable with that.
1. Acquisition offers for us are not public since informing people of an acquisition that might not happen can be very disruptive.
1. Acquisition offers we give are not public since the organization being acquired frequently prefers to have them stay private.
1. Customer information in issues: if an issue needs to contain _any_ specific information about a customer, including but not limited to company name, employee names, number of users, the issue should be made confidential. Try to avoid putting customer information in an issue by describing them instead of naming them and linking to their salesforce account.
1. Competitive sales and marketing campaign planning is confidential since we want the minize the time the competition has to respond to it.
1. Customer information is not public since customers are not comfortable with that and it would make it easier for competitors to approach our customers.
1. [Sales battlecards](https://blog.pandadoc.com/the-basics-of-battle-cards/) are not public since we want to minimize the time the competition has to respond to it. Our [feature comparisons](https://about.gitlab.com/devops-tools/) are public.
1. When we discuss a customer by name that is not public unless we're sure the customer is OK with that. When we discuss a competitor (for example in a sales call) this can be public as our competitive advantages are public.

## Be transparent
1. Work out in the **open**, try to use public issue trackers and repositories when possible.
1. Share solutions you find and answers you receive with the **whole community**.
1. If you make a mistake, don't worry, correct it and **proactively** let the affected party, your team, and the CEO know what happened, how you corrected it and how, if needed, you changed the process to prevent future mistakes.


### Access to resources
1. Respect the **privacy** of our users and your fellow GitLabbers. Secure our production data at all times. Only work with production data when this is needed to perform your job. Looking into production data for malicious reasons (for example, [LOVEINT](https://en.wikipedia.org/wiki/LOVEINT) or spying on direct messages of GitLabbers) is a fireable offense.
1. If you need to start a **new repo** or project that is intended to be maintained over the long term, make sure that you follow the [GitLab Repo Guidelines](/handbook/engineering/#gitlab-repositories).
1. If you need access to GitLab resources, make sure to follow the [GitLab Access Request Guidelines](/handbook/engineering/#gitlab-access-requests)
