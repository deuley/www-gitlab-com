---
layout: markdown_page
title: "E-group KPIs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Intro

Every executive at GitLab has Key Performance Indicators (KPIs).
This page has the goals and links to the definitions.
The goals are merged by the CEO.
The definitions should be in the relevant part of the handbook.
In the definition it should mention what the canonical source is for this indicator.

## Public

In the doc 'GitLab Metrics' at IPO are the KPIs that we may share publicly.

## CEO

CEO goals are duplicates of goals of the reports.
The CEO goals are the most important indicators of company performance.

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1
1. [Sales efficiency ratio](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1
1. [ARR](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) YoY > 190%
1. [Pipe generated](/handbook/finance/operating-metrics/#pipe-generated) vs. plan > 1
1. [Wider community contributions per release](/handbook/finance/operating-metrics/#community-contributions)
1. [CAC / LTV](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) ratio > 4
1. Average [NPS](/handbook/finance/operating-metrics/#nps)
1. [Hires](/handbook/finance/operating-metrics/#hires) vs. plan > 0.9
1. [Monthly employee turnover](/handbook/finance/operating-metrics/#turnover)
1. [New hire average score](/handbook/finance/operating-metrics/#new-hire-score)
1. [Merge Requests per release per developer](/handbook/finance/operating-metrics/#mr-release-developer)
1. [Uptime GitLab.com](/handbook/finance/operating-metrics/#uptime)
1. [Active users per hosting platform](/handbook/finance/operating-metrics/#active-users-platform): Total, AWS, Azure, GCP, IBM, Unknown
1. [Support CSAT](https://about.gitlab.com/handbook/finance/operating-metrics/#csat)
1. [Runway](/handbook/finance/operating-metrics/#runway) > 12 months
1. MAAUI (monthly active users of UI)[^1] 

[^1]: MAUUI is a [Meltano](https://meltano.com/) specific KPI and an exception to the CEO goals. MAUUI is not part of the GitLab Executive Team KPIs.

## CRO

[Looker 45](https://gitlab.looker.com/dashboards/45)

1. [IACV](https://about.gitlab.com/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1
1. [Field efficiency ratio](/handbook/finance/operating-metrics/#field-efficiency-ratio) > 2
1. [TCV](https://about.gitlab.com/handbook/finance/operating-metrics/#total-contract-value-tcv) vs. plan > 1
1. [ARR](https://about.gitlab.com/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) YoY > 190%
1. [Win rate](/handbook/finance/operating-metrics/#win-rate) > 30%
1. [% of ramped reps at or above quota](/handbook/finance/operating-metrics/#ramped-reps-quota) > 0.7
1. [Net Retention](https://about.gitlab.com/handbook/finance/operating-metrics/#retention-gross--net-dollar-weighted) > 2
1. [Gross Retention](https://about.gitlab.com/handbook/finance/operating-metrics/#retention-gross--net-dollar-weighted) > 0.9
1. [Rep IACV per comp](/handbook/finance/operating-metrics/#rep-iacv-comp) > 5
1. [ProServe](/handbook/finance/operating-metrics/#pcv) revenue vs. cost > 1.1
1. [Services attach rate](/handbook/finance/operating-metrics/#services-attach-rate) for strategic > 0.8
1. [Self-serve sales ratio](/handbook/finance/operating-metrics/#self-serve-ratio) > 0.3
1. [Licensed users](/handbook/finance/operating-metrics/#licensed-users)
1. [ARPU](/handbook/finance/operating-metrics/#arpu)
1. [New strategic accounts](/handbook/finance/operating-metrics/#new-strategic)
1. [IACV per Rep](/handbook/finance/operating-metrics/#iacv-rep) > $1.0M
1. [New hire location factor](/handbook/finance/operating-metrics/#new-hire-location-factor) < TBD

## CMO

[Looker 51](https://gitlab.looker.com/dashboards/51)

1. [Pipe generated](/handbook/finance/operating-metrics/#pipe-generated) vs. plan > 1
1. [Pipe-to-spend](/handbook/finance/operating-metrics/#pipe-to-spend) > 5
1. [Marketing efficiency ratio](/handbook/finance/operating-metrics/#marketing-efficiency-ratio) > 2
1. [SCLAU](/handbook/finance/operating-metrics/#sclau)
1. [CAC / LTV ratio](https://about.gitlab.com/handbook/finance/operating-metrics/#ltv-to-cac-ratio) > 4
1. [Twitter mentions](/handbook/finance/operating-metrics/#twitter-mentions)
1. [Sessions on our marketing site](/handbook/finance/operating-metrics/#sessions-marketing-site)
1. [New users](/handbook/finance/operating-metrics/#new-users)
1. [Product Installations](/handbook/finance/operating-metrics/#product-installations): Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates
1. [Social response time](/handbook/finance/operating-metrics/#social-response-time)
1. [Meetup Participants with GitLab presentation](/handbook/finance/operating-metrics/#meetup-participants)
1. [GitLab presentations given](/handbook/finance/operating-metrics/#gitlab-presentations)
1. [Wider community contributions per release](/handbook/finance/operating-metrics/#community-contributions)
1. [Monthly Active Contributors from the wider community](/handbook/finance/operating-metrics/#mac)
1. [New hire location factor](/handbook/finance/operating-metrics/#new-hire-location-factor) < TBD

## CCO

[Looker 44](https://gitlab.looker.com/dashboards/44)

1. [Hires](/handbook/finance/operating-metrics/#hires) vs. plan > 0.9
1. [Apply to hire days](/handbook/finance/operating-metrics/#apply-to-hire) < 30
1. No offer [NPS](/handbook/finance/operating-metrics/#nps) > [4.1](https://stripe.com/atlas/guides/scaling-eng)
1. [Offer acceptance rate](/handbook/finance/operating-metrics/#offer-acceptance-rate) > 0.9
1. Average [NPS](/handbook/finance/operating-metrics/#nps)
1. [Average location factor](/handbook/finance/operating-metrics/#avg-location-factor)
1. [New hire location factor](/handbook/finance/operating-metrics/#new-hire-location-factor) < TBD
1. Monthly [employee turnover](/handbook/finance/operating-metrics/#employee-turnover)
1. YTD [employee turnover](/handbook/finance/operating-metrics/#employee-turnover)
1. [Candidates per vacancy](/handbook/finance/operating-metrics/#candidates-per-vacancy)
1. [Percentage of vacancies with active sourcing](/handbook/finance/operating-metrics/#vacancies-active-sourcing)
1. [New hire average score](/handbook/finance/operating-metrics/#new-hire-score)
1. Onboarding [NPS](/handbook/finance/operating-metrics/#nps)
1. [Diversity lifecycle](/handbook/finance/operating-metrics/#diversity-lifecycle): applications, recruited, interviews, offers, acceptance, retention
1. [PeopleOps cost per employee](/handbook/finance/operating-metrics/#peopleops-cost-employee)
1. [Discretionary bonus](/handbook/incentives/#discretionary-bonuses) per employee per month > 0.1 

## CFO

[Looker 43](https://gitlab.looker.com/dashboards/43)

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) per [capital consumed](/handbook/finance/operating-metrics/#capital-consumption) > 2
1. [Sales efficiency](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1.0
1. [Magic number](https://about.gitlab.com/handbook/finance/operating-metrics/#magic-number) > 1.1
1. [Gross margin](/handbook/finance/operating-metrics/gross-margin) > 0.9
1. [Average days of sales outstanding](https://about.gitlab.com/handbook/finance/operating-metrics/#days-sales-outstanding-dso) < 45
1. [Average days to close](/handbook/finance/operating-metrics/#days-to-close) < 10
1. [Runway](/handbook/finance/operating-metrics/#runway) > 12 months
1. [New hire location factor](/handbook/finance/operating-metrics/#new-hire-location-factor) < 0.7
1. [ARR by annual cohort](/handbook/finance/operating-metrics/#arr-cohort)
1. [Reasons for churn](/handbook/finance/operating-metrics/#churn-reasons)
1. [Reasons for net expansion](/handbook/finance/operating-metrics/#expansion-reasons)
1. [Refunds processed as % of orders](/handbook/finance/operating-metrics/#refunds-processed-orders)


## VP of Product

[Looker 69](https://gitlab.looker.com/dashboards/75) only has MAU and SMAU.

1. [SMAU](/handbook/finance/operating-metrics/#smau)
1. [MAU](/handbook/finance/operating-metrics/#monthly-active-user-mau)
1. [Sessions on release post](/handbook/finance/operating-metrics/#sessions-release-post)
1. [Installation churn](/handbook/finance/operating-metrics/#installation-churn)
1. [User churn](/handbook/finance/operating-metrics/#user-churn)
1. [New hire location factor](/handbook/finance/operating-metrics/#new-hire-location-factor) < TBD
1. [Acquisition](https://about.gitlab.com/direction/growth/#acquistion)
1. [Adoption](https://about.gitlab.com/direction/growth/#adoption)
1. [Upsell](https://about.gitlab.com/direction/growth/#upsell)
1. [Rention](https://about.gitlab.com/direction/growth/#retention)
1. [Net Promotor Score](/handbook/finance/operating-metrics/#nps) / [Customer Satisfaction](/handbook/finance/operating-metrics/#csat) with the product

## VP of Engineering

[Looker 46](https://gitlab.looker.com/dashboards/46)

1. [Merge Requests per release per engineer in product development](/handbook/finance/operating-metrics/#mr-release-engineer-product) > 10
1. [Uptime GitLab.com](/handbook/finance/operating-metrics/#uptime) > 99.95%
1. [Performance GitLab.com](/handbook/finance/operating-metrics/#performance-gitlab)
1. Support [SLA](/handbook/finance/operating-metrics/#sla)
1. Support [CSAT](https://about.gitlab.com/handbook/finance/operating-metrics/#csat)
1. [Support cost](/handbook/finance/operating-metrics/#support-cost) vs. recurring revenue
1. [Days to fix S1 security issues](/handbook/finance/operating-metrics/#days-to-fix-issues)
1. [Days to fix S2 security issues](/handbook/finance/operating-metrics/#days-to-fix-issues)
1. [Days to fix S3 security issues](/handbook/finance/operating-metrics/#days-to-fix-issues)
1. [GitLab.com infrastructure cost per MAU](/handbook/finance/operating-metrics/#gitlab-infra-cost-mau)
1. [ARR](/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) per support rep > $1.175M
1. [New hire location factor](/handbook/finance/operating-metrics/#new-hire-location-factor) < 0.5
1. [Public Cloud Spend](/handbook/finance/operating-metrics/#public-cloud-spend)

## VP of Alliances

[Looker 53](https://gitlab.looker.com/dashboards/53) is broken.

1. [Active users per hosting platform](/handbook/finance/operating-metrics/#active-users-platform): Total, AWS, Azure, GCP, IBM, Unknown
1. [Active installations per hosting platform](/handbook/finance/operating-metrics/#active-installs-platform): Total, AWS, Azure, GCP, IBM, Unknown
1. [Product Installations](/handbook/finance/operating-metrics/#product-installations): Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates
1. [Product Downloads](/handbook/finance/operating-metrics/#product-downloads): Updates & Initial per distribution method: Omnibus, Cloud native helm chart, Source
1. [Acquistion velocity](/handbook/finance/operating-metrics/#acquisition-velocity): [Acquire 3 teams per quarter](/handbook/alliances/acquisition-offer/) for less than $2m in total.
1. [Acquistion success](/handbook/finance/operating-metrics/#acquisition-success): 70% of acquisitions ship the majority of their old product functionality as part of GitLab within 3 months after acquistion.
