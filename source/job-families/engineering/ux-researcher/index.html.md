---
layout: job_family_page
title: "UX Researcher"
---

At GitLab, UX Researchers collaborate with our UX designers, front/backend engineers, product managers and the rest of the community to assist in determining what features should be built, how they should behave, and what the priorities should be. User Experience Researchers report to the UX Lead.

## Responsibilities

All UX Researchers are responsible for:

* The upkeep and promotion of GitLab First Look.
* The development and upkeep of the UX Research sections of the handbook and the GitLab Design System.
* Writing blog posts and responding to comments about GitLab’s user experience.


#### Junior UX Researcher

A Junior UX Researcher has some practical experience but needs regular guidance and training to produce their best work and develop their skills. 

* Understands user research methods, when to use them and how to apply them correctly. This may include, but not be limited to: usability testing, user interviews, surveys and competitor analysis.
* Understands basic methods to analyze and synthesize research data. Interprets data correctly. 
* Translates research findings into actionable recommendations in conjunction with the Product team and UX Designers.
* Plans, designs and conducts usability testing and user interviews. Effectively undertakes associated tasks such as participant recruitment and scheduling, producing test materials, setting up test environments, analyzing and delivering findings.
* Assists in the creation of research deliverables such as personas, customer journey maps, etc.
* Responsible for their own personal development, actively seeks out opportunities to improve their skill set. 


#### UX Researcher

A UX Researcher is able to undertake all forms of research. They lead on research initiatives and are capable of working independently. 

* Understands and has experience of a wide range of research methods. Knows when to use those methods and how to apply them correctly. This may include, but not be limited to: usability testing, user interviews, card sorts, tree tests, surveys, competitor analysis and diary studies.
* Leads progressive, iterative research across milestones.
* Employs robust methods to analyze and synthesize research data. 
* Translates research findings into actionable recommendations in conjunction with UX Designers and the Product team. 
* Challenges assumptions, ensures UX Designers and the Product team action findings from users. 
* Instigates and suggests research projects based on the needs of UX Designers and the Product team.
* Participates in [product discovery](/handbook/product/#product-discovery-issues). Helps to generate ideas for features and improvements.
* Creates research deliverables such as personas, customer journey maps, etc.


#### Senior UX Researcher

A Senior UX Researcher is an experienced practitioner who is able to lead on more complex research studies. They mentor other UX Researchers.

* Understands and has experience of a wide range of research methods. Knows when to use those methods and how to apply them correctly. This may include, but not be limited to: usability testing, user interviews, card sorts, tree tests, surveys, competitor analysis and diary studies.
* Conducts user research for features with challenging requirements and complex user journeys.
* Advises UX Researchers on choice and use of research methods to assure best practice.
* Understands and can help UX Researchers apply a range of methods to analyze research data and synthesize findings. 
* Supports the UX Research Manager with [UX Research Group Conversations](/handbook/people-operations/group-conversations/).
* Improves the team’s workflow by contributing to processes and identifying new tools for the team to use.
* Interviews potential UX Research candidates.


## Success Criteria

You know you are doing a good job as a UX Researcher when:

* You collaborate effectively with UX Designers and Product Managers.
* You can quickly build rapport with GitLab's users.
* You complete all research you have committed to within a milestone. 
* You contribute ideas for feature improvements based on your research findings.


### UX Researcher Interview Questions <a name="ux-research-interview-questions"></a>

The UX Researcher Interview determines if a UX Researcher is a good fit for GitLab. Here are some questions we might ask:

1. What are some existing case studies or research results we can see as an example of your work?
1. When you did `x` project, what was the biggest problem that you had to solve and how did you solve it?
1. What was a really interesting insight or finding that you had from a recent project?


## Relevant links

- [UX Research Handbook](/handbook/engineering/ux/ux-research/)
- [UX Department Handbook](/handbook/engineering/ux/)
- [Engineering Handbook](/handbook/engineering)
- [Product Handbook](/handbook/product)
- [GitLab UX Research Project](https://gitlab.com/gitlab-org/ux-research)
- [GitLab Design System](https://design.gitlab.com/)


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with our Global Recruiters.
* Next, candidates will be invited to schedule a 1 hour interview with the UX Research Manager.
* Next, candidates will then be invited to schedule a 1 hour interview with a UX Designer. 
* Candidates will be invited to schedule a third 45 minute interview with our UX Director. 
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

[groups]: /company/team/structure/#groups

