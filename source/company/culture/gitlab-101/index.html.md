---
layout: markdown_page
title: "GitLab 101"
---

## GitLab 101s

### CEO 101

There is a monthly GitLab CEO 101 call with new hires and the CEO. Here we talk about the following topics. This is a zoom call that will be uploaded to YouTube. Please read the history and values links provided below and come prepared with questions. The call will be driven by questions asked by new employees. Make sure your full name is set in Zoom.

1. Questions about GitLab history [/company/history/](/company/history/)
1. Questions about our values [/handbook/values](/handbook/values)
1. Team structure [/company/team/structure/](/company/team/structure/) or [organization chart](/company/team/org-chart)
1. How we work [/handbook/general-guidelines](/handbook/general-guidelines)
  - Please pay special attention to the guideline about developing content and procedures in a public, transparent, [handbook-first](/handbook/general-guidelines/#handbook-first) manner

Note that, when thinking about what questions you'd like to ask, it’s hard to be open about financials. It’s easy when everything is going well but very hard when it’s not. We try to be open with our team as we progress, but it will hurt being open to the outside world about this in the future.

All questions and answers during the call should be added to the this page or to the relevant pages of [the handbook](/handbook/). This is done during the call to immediately make any changes; for example we created [this commit](https://gitlab.com/gitlab-com/www-gitlab-com/commit/8cf1b0117dce5439f61e207315f75db96c917056) together on the call.

The calendar invite will say: "You need to prepare for this call, please see /company/culture/gitlab-101/ for details."

Below a few examples of the GitLab 101 calls

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtube.com/embed/YovTmwsMrQo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/FALTpdV6dsw" frameborder="0" allowfullscreen></iframe>
</figure>


### New Teammates Introduction

Immediately before the CEO 101, there will be a separate meeting for new teammate introductions. This gives everyone on the call a chance to get to know each other before the Q&A starts, and saves as much time as possible for the CEO to take questions. Each introduction should take less than 60 seconds. It is surprising how much we can communicate in a minute and we want to ensure that everyone has a chance to introduce themselves. A member from the PeopleOps Team will be in attendance to ensure this happens on time and is recorded. Please introduce yourself using the order in the calendar invite and share:

  - What do you do at GitLab?
  - Why did you join GitLab?
  - What do you enjoy in your private life (quirky details are encouraged, also feel free to introduce your significant other and/or pets)?
  - Did you already have 5 [virtual coffee chats](/company/culture/all-remote/#coffee-chats)?
  - If you already had 5 are you open to more?

Please keep your introduction to less than 1 minute 30 seconds. After you're finished with your introduction, invite the next person to talk.

### People Ops 101s

There is a monthly People Ops 101 Zoom call with new hires, our Chief Culture Officer (CCO), and members of the People Ops team. It is meant to occur following GitLabbers' participation in the CEO 101. The links above are good reading for both meetings. In addition to the topics above, new hires may be interested in our culture, [people priorities](/company/okrs/), [diversity](/handbook/values/#diversity) and [inclusion](/company/culture/inclusion/), [hiring](/handbook/hiring/), and a variety of other topics. Our CCO welcomes the team's questions and comments, but also appreciates hearing suggestions and ideas from GitLab's new hires.

### Frequently Asked Questions about the GitLab Culture

##### All previously asked questions have been integrated into our [handbook](/handbook/)

Future questions will be added to this FAQ section.
