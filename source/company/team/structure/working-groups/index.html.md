---
layout: markdown_page
title: "Working Groups"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What's a Working Group?

Like all groups at GitLab, a [working group](https://en.wikipedia.org/wiki/Working_group) is an arrangement of people from different functions. What makes a working group unique is that it has defined roles and responsibilities, and is tasked with achieving a collaborative goal. Ideally a working group disbands when the goal is achieved so that GitLab doesn't accrue bureaucracy.

## Roles and Responsibilities

| Role                  | Responsibility                                                          |
|-----------------------|-------------------------------------------------------------------------|
| Facilitator           | Assembles the working group, runs the meeting, and communicates results |
| Functional Lead       | Someone who represents their function                                   |
| Member                | Any subject matter expert                                               |
| Executive Stakeholder | An executive interested in the results, or responsible for the outcome  |

## Process

* Preparation
  * Create a working group page
  * Assemble a team from required functions
  * Create an agenda doc public to the company
  * Create a Slack channel (with `#wg_` prefix) that is public to the company
  * Schedule a recurring Zoom meeting
* Define a goal
* Gather metrics that will tell you when the goal is met
* Organize activities that should provide incremental progress
* Ship iterations and track the metrics
* Communicate the results
* Disband the working group

## Active Working Groups

* [Development Metrics Working Group](/company/team/structure/working-groups/development-metrics/)
* [GitLab.com Profitability Working Group](/company/team/structure/working-groups/gitlab-com-profitability/)

## Past Working Groups

N/A
