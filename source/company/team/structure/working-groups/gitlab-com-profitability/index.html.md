---
layout: markdown_page
title: "GitLab.com Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | March 5, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_dot-com-profit](https://gitlab.slack.com/messages/CGQHT13RB) (only accessible from within the company) |
| Google Doc   | [GitLab.com Profitability Working Group Agenda](https://docs.google.com/document/d/1BmqoTrzWhNW_ytbc_H7I3BH9YgusT9AikfO7aHlNPdA/edit) (only accessible from within the company) |

## Business Goal

To make GitLab.com profitable by...

1. Increasing revenue from paid usage
1. Making the infrastructure more efficient on a per-user basis

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Eric Johnson          | VP of Engineering              |
| Infrastructure Lead   | Gerir Lopez-Fernandez | Director of Infrastructure     |
| PM Lead               | Jeremy Watson         | Product Manager, Manage        |
| Member                | Eric Brinkman         | Dir of Product Management      |
| Member                | Andrew Newdigate      | Staff Engineer, Infrastructure |
| Executive Stakeholder | Mark Pundsack         | Head of Product                |
| Executive Stakeholder | Paul Machle           | CFO                            |
